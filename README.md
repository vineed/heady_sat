# heady_sat

Heady Sat. Product Catalog

# Requirments
Node.js
MongoDB

###installation steps
1. Update local Packages (npm update)
2. Change Mongo Config (config/db.js)
3. import Mongo Schema (Schema is shared below)


## Mongo Schema
use productCat;

db.products.insert({
  _id : "111446GB4",
  title: "Sansung mobile phone",
  description: "The greatest android phone on the market .....",

  manufacture_details: {
    model_number: "j7",
    release_date: new ISODate("2017-05-17T08:14:15.656Z")
  },

  shipping_details: {
    weight: 350,
    width: 10,
    height: 10,
    depth: 1
  },

  quantity: 199,

  pricing: {
    price: 7000
  },
  categories: ['mobile/3G', 'mobile/4G']
});

db.products.ensureIndex({categories:1 });

db.categories.insert({_id: "mobile" , title: "Mobiles Phones", parent: "" })
db.categories.insert({_id: "mobile/4G" , title: "Mobiles with 4G support", parent: "mobile" });
db.categories.insert({_id: "mobile/3G" , title: "Mobiles with 3G support", parent: "mobile" });
 
db.categories.ensureIndex({parent: 1});
db.categories.ensureIndex({path: 1});

