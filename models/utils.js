/**
 * Util file for app
 */
"use strict";


var util = {
	
	getMongoConnection(callback){
		var MongoClient = require('mongodb').MongoClient;
		console.log(db_url['productCat']);
		// Use connect method to connect to the server
		MongoClient.connect(db_url['productCat'], {native_parser:true}, function(err, client) {
			console.log("Connected successfully to server");
			var db = client.db('productCat');
			callback( db ,client );
		});
	},
	
    errorHandler : function( callback ) {
        try{
            if(typeof callback != 'function')
                throw new Error("Not A Function To excute");
            callback();
        }catch(error){
			console.log(error);
            console.log('Error : ',error.message);
        }
    }
};

module.exports = util;