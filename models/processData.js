/**
 * File to process data and add filters data 
 */
var processData = {
    

	
	/**
     * Get Product\Categories List and Element details 
	 * @param STRING table
     * @param INT start_limit
     * @param INT offset
     * @returns ARRAY data from tables
     */
	getDetails : function(callback,table,query,start_limit,offset) {
		var thisObj = this;
        global.utilObj.errorHandler(function() {
			global.utilObj.getMongoConnection( function(db,client) {
				var collection = db.collection( table );
				var response = [];
				
				start_limit = typeof start_limit != "undefined" ? start_limit : 30;
				offset = typeof offset != "undefined" ? offset : 0;
				query = typeof query != "undefined" ? query : {};
				
				collection.find( query ).
				limit( start_limit ).
				skip( offset ).
				toArray( function ( err, docs ) {
					callback(err,docs);
				});
			});	
        });
	},
	
	/**
     * 
     * @param FUNCTION callback
     * @param STRING table_name
     * @param ARRAY query
     * @param ARRAY update_data
     * @returns {undefined}
     */
    updateDetails : function( callback , table_name , query , update_data ) {
        var thisObj = this;
        global.utilObj.errorHandler( function() {
            global.utilObj.getMongoConnection( function(db,client) {
				var collection = db.collection( table_name );
				
				collection.update(
					query ,
					{
						$set : update_data
					},
					{strict:false,upsert:true},
					function(err,data) {
						
						if (err)
							console.log('error - '+err);
						//console.log("update data = ",data);
					}
				);
			});
        });
    }
};

module.exports = processData;