var express = require('express');
var router = express.Router();
var processDataObj = require('../models/processData');


/* GET Product listing. */
router.get('/', function(req, res, next) {
	processDataObj.getDetails(function(err,data){
		
		if (err)
			res.send(err);
		console.log('products',data[0]);
		res.render('products',{ products_list : data } );
	},'products');
	
});

router.get('/edit/:product_id', function(req, res, next) {
	processDataObj.getDetails(function(err,data){
		if (err)
			res.send(err);
		console.log('products',data);
		res.render('product_edit',{ product_details : data[0] } );
	},'products',{ _id : req.params.product_id});
});

router.post('/edit/:product_id', function(req, res, next) {
	console.log(req.body);
	processDataObj.updateDetails(function(err,data){
		if (err)
			res.send(err);
		res.send({error:false,msg:"success"});
	},'products',{},req.body);
});


module.exports = router;
