var express = require('express');
var router = express.Router();
var processDataObj = require('../models/processData');

/* GET users listing. */
router.get('/', function(req, res, next) {
  processDataObj.getDetails(function(err,data){
		
		if (err)
			res.send(err);
		console.log('data',data);
		res.render('categories',{ categories_list : data } );
	},'categories');
});

router.get('/edit/:category_id', function(req, res, next) {
	
	var category_id = req.params.category_id.replace(/-/g,"/");
	
	processDataObj.getDetails(function(err,data){
		if (err)
			res.send(err);
		console.log(data);
		res.render('category_edit',{ category_details : data[0] } );
	},'categories',{ _id : category_id } );
});

router.post('/edit/:category_id', function(req, res, next) {
	var category_id = req.params.category_id.replace(/-/g,"/");
	console.log(req.body);
	processDataObj.updateDetails(function(err,data){
		if (err)
			res.send(err);
		res.send({error:false,msg:"success"});
	},'categories',{_id:category_id},req.body);
});

router.get('/child_category/:category_id', function(req, res, next) {
	var category_id = req.params.category_id.replace(/-/g,"/");
	processDataObj.getDetails(function(err,data){
		if (err)
			res.send(err);
		console.log(data);
		res.send({error:false,data:data});
	},'categories',{ "parent" : category_id } );
});

router.get('/products_category/:category_id', function(req, res, next) {
	var category_id = req.params.category_id.replace(/-/g,"/");
	processDataObj.getDetails(function(err,data){
		if (err)
			res.send(err);
		console.log(data);
		res.send({error:false,data:data});
	},'products',{ "categories" : category_id } );
});

module.exports = router;
